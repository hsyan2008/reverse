module gitea.com/hsyan2008/reverse

go 1.15

require (
	gitea.com/lunny/log v0.0.0-20190322053110-01b5df579c4e
	github.com/denisenkom/go-mssqldb v0.0.0-20200831201914-36b6ff1bbc10
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gobwas/glob v0.2.3
	github.com/lib/pq v1.8.0
	github.com/mattn/go-sqlite3 v1.14.2
	github.com/spf13/cobra v1.0.0
	gopkg.in/yaml.v2 v2.3.0
	xorm.io/xorm v1.0.5
)
